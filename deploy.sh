#!/bin/bash

# Deploy script for the psplash splash screen application

set -e

cd $(dirname $0)

DEFAULT_DEPLOY=../deploy/rootfs
NUM_ARGS=$#
STARTING_POINT=$PWD

if [ "$NUM_ARGS" -eq "1" ]
then
	DEPLOY_PATH="$1"
else
	DEPLOY_PATH="$DEFAULT_DEPLOY"
	echo "No path passed, using default deploy path: "$DEPLOY_PATH""
	echo ""
fi

if [ ! -e "$DEPLOY_PATH""$BIN_LOCATION" ] || [ ! -e "$DEPLOY_PATH""$LIB_LOCATION" ]
then
	echo "Required deploy path does not exist, exiting"
	exit 1
fi

if [ ! -e psplash ]
then
	echo "Target folder does not exist, a build is required before deploying, exiting"
	exit 1
fi

echo "#### Deploying bin files####"
echo ""
cp psplash-write psplash "$DEPLOY_PATH"/usr/bin
echo "#### Deploying startup scripts ####"
cp psplash.sh "$DEPLOY_PATH"/etc/init.d/
cd "$DEPLOY_PATH"/etc/rcS.d
if [ ! -e S00psplash.sh ]
then
	ln -s ../init.d/psplash.sh S00psplash.sh
	echo "" 
else
	echo "link to startup script exists moving on"
	echo ""
fi
echo "#### Create required mount point ####"
cd "$STARTING_POINT"
if [ ! -e "$DEPLOY_PATH"/mnt/.psplash ]
then
	mkdir "$DEPLOY_PATH"/mnt/.psplash
	echo ""
else
	echo "mount point exists moving on"
	echo ""
fi
echo "Placeholder for tracking purposes, hg won't track an empty folder" > "$DEPLOY_PATH"/mnt/.psplash/README

echo "Deploy psplash - COMPLETE"
echo ""

exit 0
