#!/bin/bash

set -e

cd $(dirname $0)

#DEFAULT_TOOLCHAIN_PATH=~/ti-processor-sdk-linux-am335x-evm-02.00.01.07/linux-devkit/sysroots/x86_64-arago-linux/usr/bin
#DEFAULT_TOOLCHAIN_PATH=~/ti-processor-sdk-linux-am335x-evm-05.02.00.10/linux-devkit/sysroots/x86_64-arago-linux/usr/bin
ARCHITECTURE=arm
CROSS_COMPILER=arm-linux-gnueabihf-
NUM_ARGS="$#"

if [ "$NUM_ARGS" -eq 0 ]
then
	TOOL_CHAIN_PATH="$DEFAULT_TOOLCHAIN_PATH"
	echo "No arguments passed, using default toolchain path: "$TOOL_CHAIN_PATH""
	echo ""
else
	TOOL_CHAIN_PATH="$1"
fi

#strong likelihood that we are in the right place if this file exists
if [ ! -e "$TOOL_CHAIN_PATH"/arm-linux-gnueabihf-gcc ]
then
	echo "Toolchain path $TOOL_CHAIN_PATH is not correct"
	exit 1
fi

export ARCH="$ARCHITECTURE"
export CROSS_COMPILE="$CROSS_COMPILER"
export PATH="$TOOL_CHAIN_PATH":"$PATH"

echo "#### Autoreconf ####"
echo ""

autoreconf -v -i

echo "#### Configure ####"
echo ""
( exec ./configure --host=arm-linux CC=arm-linux-gnueabihf-gcc )

echo "#### Building ####"
echo ""
make

echo "Psplash build - COMPLETE"
echo ""

exit 0


